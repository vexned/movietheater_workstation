#ifndef PERSON_H
#define PERSON_H

#include <QString>
#include <QDataStream>

enum class Privelege{Admin,Cashier,None};

class Person
{
public:
    Person(const QString &fname = "", const QString &lname = "", const QString &mname="", const QString &log="", const QString &pass="");
    Person(const Person &pers);
    Person &operator =(const Person &pers);
    void setFirstName(const QString &name);
    void setLastName(const QString &name);
    void setMiddleName(const QString &name);
    virtual QString printName(){return QString(firstName+lastName+middleName);}
    virtual Privelege privelegeCheck() {return Privelege::None;}

    friend QDataStream &operator <<(QDataStream& stream, const Person& person);
    friend QDataStream &operator >>(QDataStream& stream, Person& Person);
    QString getLogin() const;
    void setLogin(const QString &value);

    QString getPassword() const;
    void setPassword(const QString &value);

protected:
    QString firstName;
    QString lastName;
    QString middleName;
    QString login;
    QString password;
    Privelege privelege;
};

#endif // PERSON_H
