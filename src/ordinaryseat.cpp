#include "ordinaryseat.h"

OrdinarySeat::OrdinarySeat(int sn, SeatState state) : Seat(sn,state)
{ 
    cost = 300;
}

OrdinarySeat::OrdinarySeat(const OrdinarySeat &seat) : Seat(seat.seatNumber, seat.state)
{
    this->cost = seat.cost;
}

QString OrdinarySeat::seatType()
{
    return QString("Обычное");
}
