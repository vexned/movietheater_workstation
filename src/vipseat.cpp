#include "vipseat.h"

VIPSeat::VIPSeat(int sn, SeatState state) : Seat(sn,state)
{
    cost = 500;
}

VIPSeat::VIPSeat(const VIPSeat &seat) : Seat(seat.seatNumber, seat.state)
{
    this->cost = seat.cost;
}

QString VIPSeat::seatType()
{
    return QString("VIP");
}
