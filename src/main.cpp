#include "mainwindow.h"
#include "adminwindow.h"
#include <QApplication>
#include <QFile>
#include "displayobject.h"
#include "movie.h"
#include "cinemahall.h"
#include "session.h"
#include "editsessiondialog.h"
#include "logindialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle(QObject::tr("АРМ Кассира кинотеатра"));
    w.show();
    return a.exec();
}
