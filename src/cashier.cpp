#include "cashier.h"

Cashier::Cashier()
    : Person()
{
    CashForDay = 0;
}

Cashier::Cashier(const QString &fn="", const QString &ln="", const QString &mn="", const QString &lg="", const QString &pw=""):
    Person(fn, ln, mn, lg, pw)
{
    CashForDay = 0;
}

Cashier::Cashier(const Cashier &cashier)
    : Person(cashier.firstName,cashier.lastName,cashier.middleName,cashier.login,cashier.password)
{
}

Cashier &Cashier::operator =(const Cashier &cashier)
{
    this->firstName = cashier.firstName;
    this->lastName = cashier.lastName;
    this->middleName = cashier.middleName;
    this->login = cashier.login;
    this->password = cashier.password;

    return *this;
}

void Cashier::addCash(const double &cash)
{
    CashForDay += cash;
}

void Cashier::removeCash(const double &cash)
{
    CashForDay -= cash;
}

QString Cashier::printName()
{
    return QString(firstName+" "+middleName+" "+lastName);
}

double Cashier::getCashForDay() const
{
    return CashForDay;
}

void Cashier::setCashForDay(double value)
{
    CashForDay = value;
}

QDataStream &operator <<(QDataStream &stream, const Cashier& person)
{
    return stream << (Person)person;
}

QDataStream &operator >>(QDataStream &stream, Cashier& person)
{
    return stream >> static_cast<Person&>(person);
}

