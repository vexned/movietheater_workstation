#include "adminwindow.h"
#include "ui_adminwindow.h"

AdminWindow::AdminWindow(QWidget *parent, QString filename) :
    QMainWindow(parent),
    ui(new Ui::AdminWindow)
{
    ui->setupUi(this);
    currentDate = QDate::currentDate();
    loadDataBase(filename);
    ui->dateEdit->setDate(currentDate);
    //Установить текущий список сеансов
    setCurrentList();
}

AdminWindow::~AdminWindow()
{
    delete ui;
}

void AdminWindow::setCurrentSession(QModelIndex selection)
{
    sessionSelectionIndex = selection;
    //Получаем сеанс из модели
    sessionSelected = sessionModel->data(selection,SessionModelRole::getDataRole).value<Session>();
    //connect(ui->sessionView->selectionModel()),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(setButtonState()));
}

void AdminWindow::insertSessionInModel(QModelIndex idx, Session *session)
{
    //sessionModel->insertRow(sessionModel->rowCount(), idx);
    sessionModel->setData(idx,QVariant::fromValue(*session),Qt::EditRole);
}

void AdminWindow::setButtonState()
{
    ui->editSession->setEnabled(ui->sessionView->selectionModel()->hasSelection());
    ui->deleteSession->setEnabled(ui->sessionView->selectionModel()->hasSelection());
}

void AdminWindow::on_dateEdit_userDateChanged(const QDate &date)
{
    currentDate = date;
    setCurrentList();
    setButtonUnactive();
}

void AdminWindow::on_addSesion_clicked()
{
    Session *temp = new Session();
    EditSessionDialog* win = new EditSessionDialog(temp, this);
    connect(win, &EditSessionDialog::applyChanges, [=](){
       sessionModel->insertRow(sessionModel->rowCount(), sessionSelectionIndex);
       sessionModel->setData(sessionModel->index(sessionModel->rowCount()-1,0),QVariant::fromValue(*temp),Qt::EditRole);
       sessionHashSet.insert(currentDate, *temp);
       delete temp;
    });
    win->exec();
}

void AdminWindow::on_editSession_clicked()
{
    Session temp = sessionSelected;
    EditSessionDialog* win = new EditSessionDialog(&sessionSelected, this);
    connect(win, &EditSessionDialog::applyChanges, [=](){
       insertSessionInModel(sessionSelectionIndex, &sessionSelected);
       sessionHashSet.remove(currentDate,temp);
       sessionHashSet.insert(currentDate, sessionSelected);
    });
    win->exec();
}

void AdminWindow::on_saveDatabaseAction_triggered()
{
    filename = QFileDialog::getSaveFileName(this,tr("Save session database"),QString(),tr("Database files (*.dat)"));
    if(filename.size()>0)
    {
        QFile file(filename);
        file.open(QIODevice::WriteOnly);
        QDataStream stream(&file);
        stream << sessionHashSet;
    }
}


void AdminWindow::loadDataBase(QString &name)
{
    if(name.size() <= 0){
        name = QFileDialog::getOpenFileName(this,tr("Open session database"),QString(),tr("Database files (*.dat)"));
    }
    if(name.size()>0)
    {
        QFile file(name);
        file.open(QIODevice::ReadOnly);
        QDataStream stream(&file);
        stream >> sessionHashSet;
    }
    name = "";
    setButtonUnactive();
    setCurrentList();
}


void AdminWindow::on_loadDatabaseAction_triggered()
{
    loadDataBase(filename);
}

void AdminWindow::on_closeAdminWindowAction_triggered()
{
    this->close();
}

void AdminWindow::on_deleteSession_clicked()
{
    sessionModel->removeRow(sessionSelectionIndex.row(), QModelIndex());
}

void AdminWindow::setCurrentList()
{
    //Если текущая модель СЕАНСА НЕ указывает на nullptr, очищаем память, отсоединяем сигнал
    if(sessionModel != nullptr) {
        disconnect(ui->sessionView->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(setButtonState()));
        disconnect(ui->sessionView->selectionModel(),SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),this, SLOT(setCurrentSession(QModelIndex)));
        delete sessionModel;
        sessionModel = nullptr;
    }
    //Берем список на текущий день
    sessionList =  sessionHashSet.values(currentDate);
    //Создаем модель из списка
    sessionModel = new SessionModel(sessionList);
    //Ставим модель в список
    ui->sessionView->setModel(sessionModel);
    //Присоединяем сигнал к слоту
    connect(ui->sessionView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(setCurrentSession(QModelIndex)));
    connect(ui->sessionView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(setButtonState()));
}

void AdminWindow::setButtonUnactive()
{
    ui->editSession->setEnabled(false);
    ui->deleteSession->setEnabled(false);
}
