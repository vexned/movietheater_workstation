#ifndef EMPTYLISTVIEW_H
#define EMPTYLISTVIEW_H

#include <QListView>

#include <QtWidgets>

class EmptyListView : public QListView {

   void paintEvent(QPaintEvent *e) {
      QListView::paintEvent(e);
      if (model() && model()->rowCount(rootIndex()) > 0) return;
      // The view is empty.
      QPainter p(this->viewport());
      p.drawText(rect(), Qt::AlignCenter, "No Items");
   }
public:
   explicit EmptyListView(QWidget *parent = 0);
   //explicit ListView(QObject &parent = 0);
};
#endif // EMPTYLISTVIEW_H
