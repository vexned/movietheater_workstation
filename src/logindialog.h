#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QList>
#include <QDebug>
#include "person.h"
#include "cashier.h"
#include "administrator.h"

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(int &id ,QWidget *parent = 0);
    ~LoginDialog();

signals:
    SuccessEnter();
private slots:
    void on_buttonBox_accepted();

private:
    Ui::LoginDialog *ui;
    QString login;
    QString password;
    QList<Person*> personList;
};

#endif // LOGINDIALOG_H
