#include "administrator.h"

Administrator::Administrator() : Person()
{
}

Administrator::Administrator(const QString &fn, const QString &ln, const QString &mn, const QString &lg, const QString &pw) : Person(fn,ln,mn,lg,pw)
{
}

Administrator::Administrator(const Administrator &admin)
    : Person(admin.firstName,admin.lastName,admin.middleName,admin.login,admin.password)
{
}

Administrator &Administrator::operator =(const Administrator &admin)
{
    this->firstName = admin.firstName;
    this->lastName = admin.lastName;
    this->middleName = admin.middleName;
    this->login = admin.login;
    this->password = admin.password;

    return *this;
}

QString Administrator::printName()
{
    return QString(firstName+" "+lastName);
}

QDataStream &operator >>(QDataStream &stream, Administrator &admin)
{
    return stream >> static_cast<Person&>(admin);
}

QDataStream &operator <<(QDataStream &stream, const Administrator &admin)
{
    return stream << (Person)admin;
}
