#include "sessionmultihashmodel.h"

SessionMultiHashModel::SessionMultiHashModel(QMultiHash<QDate, Session> hashme, QObject *parent)
    : QAbstractListModel(parent)
{
    this->hash = hashme;
}

QVariant SessionMultiHashModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
        return QString("Column %1").arg(section);
    else
        return QString("Row %1").arg(section);
}

bool SessionMultiHashModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}

int SessionMultiHashModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return hash.size();
}

QVariant SessionMultiHashModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= hash.size())
        return QVariant();

    if (role == Qt::DisplayRole)
        return hash.
    else
        return QVariant();
}

bool SessionMultiHashModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags SessionMultiHashModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool SessionMultiHashModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
}

bool SessionMultiHashModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
}
