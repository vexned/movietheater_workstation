#include "displayobject.h"

DisplayObject::DisplayObject(const QString &name)
{
    objectName = name;
}

DisplayObject::DisplayObject(const DisplayObject &object)
{
    objectName = object.objectName;
}

void DisplayObject::setName(const QString &name)
{
    objectName = name;
}

QString DisplayObject::getName()
{
    return objectName;
}

QDataStream &operator >>(QDataStream &stream, DisplayObject &obj)
{
    return stream >> obj.objectName;
}

QDataStream &operator <<(QDataStream &stream, const DisplayObject &obj)
{
    return stream << obj.objectName;
}
