#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(int &id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    int *idp = &id;
    ui->setupUi(this);
    personList.append(new Administrator("Вася","Васин","Васинов","vasya","qwerty"));
    personList.append(new Cashier("Володя","Володев","Володин","volya","12345"));
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_buttonBox_accepted()
{
    login = ui->LoginField->text();
    password = ui->PassField->text();
    for(int i = 0; i < personList.size(); i++)
    {
        if(login == personList[i]->getLogin())
            if(password == personList[i]->getPassword())
            {
                *idp = i;
                emit SuccessEnter();
                return;
            }
    }
}
