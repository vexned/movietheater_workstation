#include "sessionmodel.h"

int SessionModel::rowCount(const QModelIndex &parent) const
{
    //Количество элементов в текущем списке сеансов
    return sessionList.count();
}

QVariant SessionModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    if (index.row() >= sessionList.size())
        return QVariant();

    //Выводим информацию о сеансе, при корректных входных данных
    if (role == Qt::DisplayRole)
        return sessionList.at(index.row()).getSessionInfo();
    else if(role == SessionModelRole::getDataRole) {
        return QVariant::fromValue(sessionList.at(index.row()));
    }
    else
        return QVariant();

    return QVariant();
}

QVariant SessionModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    //Неиспользуемый метод, но необходимый к перегрузке
    //В нашей модели нет заголовков
    if (role != Qt::DisplayRole)
        return QVariant();
    return QVariant();
}

bool SessionModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    //Установка значений в модель
    if(role == Qt::EditRole){
        sessionList[index.row()] = value.value<Session>();
    }
    if(role == SessionModelRole::setHallRole) {
        sessionList[index.row()].setCinemaHall(value.value<CinemaHall>());
    }
    return true;
}

bool SessionModel::insertRows(int row, int count, const QModelIndex &parent)
{
    //Метод вставки рядок,
    //вставляем элементы в конец списка
    beginResetModel();
    beginInsertRows(parent,row,row+count-1);
    for(int i = 0; i < count; i++)
    {
        sessionList.insert(sessionList.size(),Session());
    }
    endInsertRows();
    endResetModel();

    emit layoutChanged();
    return true;
}

bool SessionModel::removeRows(int row, int count, const QModelIndex &parent)
{
    //Метод удаления рядов,
    beginResetModel();
    beginRemoveRows(parent,row,row+count-1);
    for(int i = row; i < count+row; i++)
    {
        sessionList.removeAt(i);
    }
    endRemoveRows();
    endResetModel();

    emit layoutChanged();
    return true;
}
