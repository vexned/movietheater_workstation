#ifndef SESSIONMODEL_H
#define SESSIONMODEL_H

#include <QAbstractListModel>
#include "session.h"
#include <QList>

enum SessionModelRole {
    getDataRole = Qt::UserRole+1,
    setHallRole = Qt::UserRole+2,
    removeSession = Qt::UserRole+3
};

class SessionModel : public QAbstractListModel
{
    Q_OBJECT

public:
    //Модель для отображения списка сеансов
    SessionModel(QList<Session> &slist, QObject *parent = 0)
        : QAbstractListModel(parent), sessionList(slist) {}

    //Перегруженные виртуальные методы
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role=Qt::EditRole);
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);

private:
    QList<Session> sessionList;
};

#endif // SESSIONMODEL_H
