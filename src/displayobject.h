#ifndef DISPLAYOBJECT_H
#define DISPLAYOBJECT_H

#include <QString>
#include <QDataStream>

class DisplayObject
{
public:
    DisplayObject(const QString &name = "");
    DisplayObject(const DisplayObject &object);
    void setName(const QString &name);
    virtual QString getName();
    friend QDataStream &operator <<(QDataStream &stream, const DisplayObject &obj);
    friend QDataStream &operator >>(QDataStream &stream, DisplayObject &obj);

protected:
    QString objectName;
};

#endif // DISPLAYOBJECT_H
