#ifndef ORDINARYSEAT_H
#define ORDINARYSEAT_H

#include "seat.h"

class OrdinarySeat : public Seat
{
public:
    OrdinarySeat(int sn = 0, SeatState state = SeatState::Avalaible);
    OrdinarySeat(const OrdinarySeat &seat);
    virtual QString seatType();
};

#endif // ORDINARYSEAT_H
