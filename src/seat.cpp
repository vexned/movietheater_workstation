#include "seat.h"

Seat::Seat(int sn, SeatState state, int cost)
{
    this->seatNumber = sn;
    this->state = state;
    this->cost = cost;
}

Seat::Seat(const Seat &seat)
{
    seatNumber = seat.seatNumber;
    state = seat.state;
    cost = seat.cost;
}

int Seat::getSeatNumber() const
{
    return seatNumber;
}

SeatState Seat::getSeatState() const
{
    return state;
}

void Seat::setState(SeatState state)
{
    this->state = state;
}

bool Seat::operator==(const Seat &r)
{
    return (this->seatNumber == r.seatNumber && this->state == r.state && this->cost == r.cost)? true : false;
}

QString Seat::seatStatusStr(SeatState state)
{
    return stateStr[static_cast<int>(state)];
}

int Seat::getCost() const
{
    return cost;
}

void Seat::setSeatNumber(int value)
{
    seatNumber = value;
}

void Seat::setCost(int value)
{
    cost = value;
}

QDataStream &operator>>(QDataStream &stream, Seat &seat)
{
    int st = static_cast<int>(seat.state);
    stream >> seat.seatNumber;
    stream >> st;
    seat.state = static_cast<SeatState>(st);
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const Seat &seat)
{
    stream << seat.seatNumber ;//<< //static_cast<SeatState>(seat.state);
    int st = static_cast<int>(seat.state);
    stream << st;
    return stream;
}
