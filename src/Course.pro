#-------------------------------------------------
#
# Project created by QtCreator 2016-12-12T00:16:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Course
TEMPLATE = app
CONFIG += c++11




SOURCES += main.cpp\
        mainwindow.cpp \
    displayobject.cpp \
    movie.cpp \
    seat.cpp \
    cinemahall.cpp \
    session.cpp \
    sessionmodel.cpp \
    cinemahallmodel.cpp \
    adminwindow.cpp \
    editsessiondialog.cpp \
    emptylistview.cpp

HEADERS  += mainwindow.h \
    displayobject.h \
    movie.h \
    seat.h \
    cinemahall.h \
    session.h \
    sessionmodel.h \
    cinemahallmodel.h \
    adminwindow.h \
    editsessiondialog.h \
    emptylistview.h

FORMS    += mainwindow.ui \
    adminwindow.ui \
    addsessiondialog.ui \
    editsessiondialog.ui
