#include "editsessiondialog.h"
#include "ui_editsessiondialog.h"

EditSessionDialog::EditSessionDialog(Session* session, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditSessionDialog)
{
    //Установка указателя на текущий сеанс
    curS = session;
    ui->setupUi(this);

    //Установка значений ComboBox, для выбора возрастного ограничения
    comboValues.insert("0+",AgeRestriction::ZeroPlus);
    comboValues.insert("6+",AgeRestriction::SixPlus);
    comboValues.insert("12+",AgeRestriction::TwelvePlus);
    comboValues.insert("16+", AgeRestriction::SixteenPlux);
    comboValues.insert("18+",AgeRestriction::EighteenPlus);
    ui->RestrictionComboBox->addItems((QStringList)(comboValues.keys()));

    //Установка значений из редактируемого объекта
    ui->MovieNameEdit->setText(curS->getMovie().getName());
    ui->durationEdit->setText(QString::number(curS->getMovie().getDuration()));
    ui->RestrictionComboBox->setCurrentText(comboValues.key(curS->getMovie().getRestriction()));

    ui->CinemaHallColumnsEdit->setText(QString::number(curS->getCinemaHall().getCols()));
    ui->CinemaHallRowsEdit->setText(QString::number(curS->getCinemaHall().getRows()));
    ui->CinemaHallNameEdit->setText(curS->getCinemaHall().getName());
    ui->timeEdit->setTime(curS->getTime());
}

EditSessionDialog::~EditSessionDialog()
{
    delete ui;
}


//Установка значений при нажатии на кнопку ОК
void EditSessionDialog::on_buttonBox_accepted()
{
    movie.setName(ui->MovieNameEdit->text());
    movie.setDuration(ui->durationEdit->text().toInt());
    movie.setRestriction(comboValues.value(ui->RestrictionComboBox->currentText()));
    curS->setMovie(movie);


    int cols = ui->CinemaHallColumnsEdit->text().toInt();
    int rows = ui->CinemaHallRowsEdit->text().toInt();
    QString name = ui->CinemaHallNameEdit->text();
    hall = CinemaHall(cols,rows,name);
    curS->setCinemaHall(hall);
    curS->setTime(ui->timeEdit->time());
    //Вызываем сигнал успешного завершения установки значений
    //Сигнал связан со слотом установки значения в модель и хеш-таблицу
    emit applyChanges();
}
