#ifndef VIPSEAT_H
#define VIPSEAT_H

#include "seat.h"

class VIPSeat : public Seat
{
public:
    VIPSeat(int sn = 0, SeatState state = SeatState::Avalaible);
    VIPSeat(const VIPSeat &seat);
    virtual QString seatType();
};

#endif // VIPSEAT_H
