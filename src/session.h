#ifndef SESSION_H
#define SESSION_H

#include "movie.h"
#include "cinemahall.h"
#include <QDateTime>

class Session
{
private:
    Movie movie; //Фильм в текущем сеансе
    mutable CinemaHall hall; //Кинозал в текущем сеансе
    QTime time; // Время в текущем сеансе
public:
    Session(Movie movie, CinemaHall hall, QTime time);
    Session();
    QString getSessionInfo() const;
    Movie getMovie() const;
    CinemaHall getCinemaHall() const;
    QTime getTime() const;
    void setCinemaHall(CinemaHall c) const;
    bool operator ==(const Session& r);
    void setMovie(const Movie &value);
    void setTime(const QTime &value);
    Session &operator=(const Session& session);

    friend QDataStream &operator <<(QDataStream &stream, const Session &session);
    friend QDataStream &operator >>(QDataStream &stream, Session &session);
};

Q_DECLARE_METATYPE(Session)

#endif // SESSION_H
