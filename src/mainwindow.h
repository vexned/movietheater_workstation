#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMultiHash>
#include <QListWidgetItem>
#include "session.h"
#include "sessionmodel.h"
#include "cinemahallmodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    //Сменить текущую сеанс
    void setCurrentSession(const QModelIndex &a);
    //Установить состояние кнопок продажи билетов
    void setButtonState();
    //Установить текущее место как выделенное
    void setCurrentSeat(QModelIndex currentSelection);
protected:
    //Переопределенная функция события таймера.
    //Необходима для отсчета времени
    void timerEvent(QTimerEvent *event);
private slots:

    void on_listView_clicked(const QModelIndex &index);
    //Сменить список по смене даты
    void on_datePicker_userDateChanged(const QDate &date);
    //Слоты для кнопок и действий по билетам
    void on_sellButton_clicked();

    void on_bookButton_clicked();

    void on_restoreButton_clicked();

    void on_sellAction_triggered();

    void on_bookAction_triggered();

    void on_restoreAction_triggered();
    //Слоты для действий настроек
    void on_loadSessionDatabase_triggered();

    void on_saveSessionDatabase_triggered();

    void on_adminPanel_triggered();

    void on_CloseARMAction_triggered();

signals:
    //Сигнал вызываемый для обновления кнопок
    void updateButton();

private:
    Ui::MainWindow *ui;
    QMultiHash<QDate, Session> sessionHashSet; //Хэш-таблица, ключом является QDate(Дата), значения Session(сеансы)
    QList<Session> sessionList; // Список на текущий день
    void setCurrentList(); // Метод установки текущего списка по Дате
    QDate currentDate; //Текущая дата, по умолчанию сегодня
    //Данные указатели будут хранить адреса объектов типа SessionModel и CinemaHallModel соотвествено
    //Благодаря этому демонстрируется механизм динамического связывания через виртуальные функции
    QAbstractListModel *sessionModel = nullptr; // текущая модель для списка, задается в setCurrentList
    QAbstractTableModel *hallModel = nullptr; //Текущая модель для таблицы рядов кинозала
    CinemaHall curHall; //Текущий кинозал
    Session session; //Текущий сеанс
    Seat seatSelected; //Выделенное место
    QModelIndex seatSelectionIndex; //Индекс выделенного места в hallModel
    QModelIndex sessionSelectionIndex; //Индекс выделенного сеанса в sessionModel
    QString filename; //Имя файла базы данных
    void setButtonUnactive(); //Установка всех кнопок и дейсвтий по билетам неактивными
    void changeSeatState(SeatState state); //смена "состояния" выделенного места
};

#endif // MAINWINDOW_H
