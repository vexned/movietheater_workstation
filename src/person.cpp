#include "person.h"

Person::Person(const QString &fname, const QString &lname, const QString &mname, const QString &log, const QString &pass)
{
    firstName = fname;
    lastName = lname;
    middleName = mname;
    login = log;
    password = pass;
}

Person::Person(const Person &pers)
{
    this->firstName = pers.firstName;
    this->lastName = pers.lastName;
    this->middleName = pers.middleName;
    this->login = pers.login;
    this->password = pers.password;
}

Person &Person::operator =(const Person &pers)
{
    this->firstName = pers.firstName;
    this->lastName = pers.lastName;
    this->middleName = pers.middleName;
    this->login = pers.login;
    this->password = pers.password;
}

void Person::setFirstName(const QString &name)
{
    firstName = name;
}

void Person::setLastName(const QString &name)
{
    lastName = name;
}

void Person::setMiddleName(const QString &name)
{
    middleName = name;
}

QString Person::getLogin() const
{
    return login;
}

void Person::setLogin(const QString &value)
{
    login = value;
}

QString Person::getPassword() const
{
    return password;
}

void Person::setPassword(const QString &value)
{
    password = value;
}

QDataStream &operator <<(QDataStream &stream, const Person &person)
{
    return stream << person.firstName << person.lastName << person.middleName;
}

QDataStream &operator >>(QDataStream &stream, Person &person)
{
    return stream >> person.firstName >> person.lastName >> person.middleName;
}
