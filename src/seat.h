#ifndef SEAT_H
#define SEAT_H
#include <QMetaType>
#include <QString>
#include <QDataStream>
enum class SeatState {Avalaible, Bought, Booked}; //Состояние места: свободно, куплено, забронировано
class Seat
{
public:
    Seat(int sn = 0, SeatState state = SeatState::Avalaible, int cost = 300); //Конструктор по умолчанию
    Seat(const Seat &seat);//Конструктор копирования
    int getSeatNumber() const;
    SeatState getSeatState()const;
    void setState(SeatState state);
    bool operator==(const Seat&r);
    QString seatStatusStr(SeatState state); //Строковое представление состояния места
    int getCost() const;

    friend QDataStream &operator<<(QDataStream &stream, const Seat &seat);
    friend QDataStream &operator>>(QDataStream &stream, Seat &seat);

    void setSeatNumber(int value);

    void setCost(int value);

private:
    int seatNumber; //Номер места
    SeatState state; //Состояние места
    QString stateStr[3] = {"Свободно", "Куплено", "Забронировано"}; //Стрковое представление состояния места
protected:
    int cost; //Цена места
};
Q_DECLARE_METATYPE( Seat )
#endif // SEAT_H
