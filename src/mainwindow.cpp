#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sessionmodel.h"
#include "adminwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //Установка текущей даты
    currentDate = QDate::currentDate();
    ui->datePicker->setDate(currentDate);
    //Установить текущий список сеансов
    setCurrentList();
    connect(this,SIGNAL(updateButton()),this,SLOT(setButtonState()));
    startTimer(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setCurrentSession(const QModelIndex &a)
{
    sessionSelectionIndex = a;
    //Получаем сеанс из модели
    session = sessionModel->data(a,SessionModelRole::getDataRole).value<Session>();
    ui->cinemaHallLabel->setText(session.getCinemaHall().getName());
    ui->filmLabel->setText(session.getMovie().getName());
    ui->sessionLabel->setText(session.getTime().toString("hh:mm"));
    ui->durationLabel->setText(QTime::fromString("00:00:00","hh:mm:ss").addSecs(session.getMovie().getDuration()*60).toString("hh:mm"));
    //Очищаем модель кинозала, если таковая есть
    if(hallModel != nullptr) {
        setButtonUnactive();
        disconnect(ui->cinemaHallTable->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(setButtonState()));
        delete hallModel;
        hallModel = nullptr;
    }
    //Получаем кинозал из текущего сеанса
    curHall = session.getCinemaHall();//Передача по ссылке? Конст?

    //Ставим модель кинозала
    //Демонстрация полиморфизма
    //Динамическое связывание
    //Указатель hallModel имеет тип QAbstractTableModel
    //Т.е. реализация виртуальных методов будет взята из класса SessionModel
    hallModel = new CinemaHallModel(curHall);
    ui->cinemaHallTable->setModel(hallModel);
    //Атрибуты единичного выделения
    ui->cinemaHallTable->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->cinemaHallTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    //Стгналы-слоты смены места и состояния кнопок
    connect(ui->cinemaHallTable->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(setButtonState()));
    connect(ui->cinemaHallTable->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(setCurrentSeat(QModelIndex)));
}

//Установка состояния кнопок в зависимости от выделения
void MainWindow::setButtonState(){
    Seat s = ui->cinemaHallTable->model()->data(seatSelectionIndex, CinemaHallModelRole::getSeat).value<Seat>();
    if(s.getSeatState() == SeatState::Avalaible)
    {
        ui->sellButton->setEnabled(true);
        ui->bookButton->setEnabled(true);
        ui->restoreButton->setEnabled(false);

        ui->sellAction->setEnabled(true);
        ui->bookAction->setEnabled(true);
        ui->restoreAction->setEnabled(false);
    }
    if(s.getSeatState() == SeatState::Bought)
    {
        ui->sellButton->setEnabled(false);
        ui->bookButton->setEnabled(false);
        ui->restoreButton->setEnabled(true);

        ui->sellAction->setEnabled(false);
        ui->bookAction->setEnabled(false);
        ui->restoreAction->setEnabled(true);
    }
    if(s.getSeatState() == SeatState::Booked)
    {
        ui->sellButton->setEnabled(true);
        ui->bookButton->setEnabled(false);
        ui->restoreButton->setEnabled(false);
        ui->sellAction->setEnabled(true);
        ui->bookAction->setEnabled(false);
        ui->restoreAction->setEnabled(false);
    }

}
//Установка текущего выделенного места
void MainWindow::setCurrentSeat(QModelIndex currentSelection)
{
    seatSelectionIndex = currentSelection;
    seatSelected = hallModel->data(currentSelection,CinemaHallModelRole::getSeat).value<Seat>();
    ui->costLabel->setText(QString::number(seatSelected.getCost()));
    ui->seatLabel->setText(QString::number(seatSelected.getSeatNumber()+1));
    ui->statusLabel->setText(seatSelected.seatStatusStr(seatSelected.getSeatState()));
}

void MainWindow::setCurrentList()
{
    //Если текущая модель КИНОЗАЛА не пуста, чистим память
    if(hallModel != nullptr)
    {
        delete hallModel;
        hallModel = nullptr;
    }
    //Если текущая модель СЕАНСА НЕ указывает на nullptr, очищаем память, отсоединяем сигнал
    if(sessionModel != nullptr) {
        disconnect(ui->listView->selectionModel(),SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),this, SLOT(setCurrentSession(QModelIndex)));
        delete sessionModel;
        sessionModel = nullptr;
    }
    //Берем список на текущий день
    sessionList =  sessionHashSet.values(currentDate);
    //Если фильмов нет, выходим
    if(sessionList.size() == 0) return;
    //Создаем модель из списка
    //Демонстрация полиморфизма
    //Динамическое связывание
    //Указатель sessionModel имеет тип QAbstractListModel
    //Т.е. реализация виртуальных методов будет взята из класса SessionModel
    sessionModel = new SessionModel(sessionList);
    //Ставим модель в список
    ui->listView->setModel(sessionModel);
    //Присоединяем сигнал к слоту
    connect(ui->listView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(setCurrentSession(QModelIndex)));
    //Установка значений места по умолчанию
    ui->cinemaHallLabel->setText("");
    ui->filmLabel->setText("");
    ui->durationLabel->setText("");
    ui->sessionLabel->setText("");
    ui->costLabel->setText("");
    ui->statusLabel->setText("");
}

//Установка неактивного состояния для кнопок
void MainWindow::setButtonUnactive()
{
    ui->bookButton->setEnabled(false);
    ui->sellButton->setEnabled(false);
    ui->restoreButton->setEnabled(false);
    ui->sellAction->setEnabled(false);
    ui->bookAction->setEnabled(false);
    ui->restoreAction->setEnabled(false);
}


//Смена состояния места в кинозале
void MainWindow::changeSeatState(SeatState state)
{
    Session replacing = session;
    Seat changableSeat = hallModel->data(seatSelectionIndex,CinemaHallModelRole::getSeat).value<Seat>();
    changableSeat.setState(state);
    hallModel->setData(seatSelectionIndex,QVariant::fromValue(changableSeat),CinemaHallModelRole::changeSeat);
    sessionModel->setData(sessionSelectionIndex, hallModel->data(QModelIndex(),CinemaHallModelRole::getHall),SessionModelRole::setHallRole);

    sessionHashSet.remove(currentDate, replacing);
    sessionHashSet.insert(currentDate, sessionModel->data(sessionSelectionIndex, SessionModelRole::getDataRole).value<Session>());
    session = sessionModel->data(sessionSelectionIndex, SessionModelRole::getDataRole).value<Session>();

    emit updateButton();
}

void MainWindow::on_listView_clicked(const QModelIndex &index)
{
    QVariant var = sessionModel->data(index, Qt::DisplayRole);
}


void MainWindow::on_datePicker_userDateChanged(const QDate &date)
{
    //Меняем текущую дату
    currentDate = date;
    //Меняем список
    setCurrentList();
}

//Установка значений времени по таймеру(1мс)
void MainWindow::timerEvent(QTimerEvent * event)
{
    ui->timeLabel->setText(QTime::currentTime().toString("hh:mm"));
}


//Купля, бронирование, возврат мест
void MainWindow::on_sellButton_clicked()
{
    changeSeatState(SeatState::Bought);
}

void MainWindow::on_bookButton_clicked()
{
    changeSeatState(SeatState::Booked);
}

void MainWindow::on_restoreButton_clicked()
{
    changeSeatState(SeatState::Avalaible);
}

void MainWindow::on_sellAction_triggered()
{
    changeSeatState(SeatState::Bought);
}

void MainWindow::on_bookAction_triggered()
{
    changeSeatState(SeatState::Booked);
}


void MainWindow::on_restoreAction_triggered()
{
    changeSeatState(SeatState::Avalaible);
}

//Нажатие Настройки->загрузить базу
void MainWindow::on_loadSessionDatabase_triggered()
{
    filename = QFileDialog::getOpenFileName(this,tr("Open session database"),QString(),tr("Database files (*.dat)"));
    if(filename.size()>0)
    {
        QFile file(filename);
        file.open(QIODevice::ReadOnly);
        QDataStream stream(&file);
        stream >> sessionHashSet;
    }
    setButtonUnactive();
    setCurrentList();
}

//Нажатие Настройки->Сохранить базу
void MainWindow::on_saveSessionDatabase_triggered()
{
    filename = QFileDialog::getSaveFileName(this,tr("Save session database"),QString(),tr("Database files (*.dat)"));
    if(filename.size()>0)
    {
        QFile file(filename);
        file.open(QIODevice::WriteOnly);
        QDataStream stream(&file);
        stream << sessionHashSet;
    }
}

//Нажатие Настройки->Панель администрирования
void MainWindow::on_adminPanel_triggered()
{
    AdminWindow* adminPanel = new AdminWindow(this,filename);
    adminPanel->setWindowTitle(tr("Панель администрирования сеансов"));
    adminPanel->show();
}

//Нажатие Настройки->Закрыть
void MainWindow::on_CloseARMAction_triggered()
{
    this->close();
}
