#ifndef ADMINWINDOW_H
#define ADMINWINDOW_H

#include <QMainWindow>
#include <QListView>
#include <QList>
#include "session.h"
#include "sessionmodel.h"
#include "editsessiondialog.h"

#include "testmodel.h"

namespace Ui {
class AdminWindow;
}

class AdminWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AdminWindow(QWidget *parent = 0, QString filename="");
    ~AdminWindow();



    void loadDataBase(QString &name);
public slots:
    //Установка текущего сеанс по выбору в таблице
    void setCurrentSession(QModelIndex selection);
    void insertSessionInModel(QModelIndex idx, Session *session);
    void setButtonState();

private slots:
    //При смене даты меняем список сеансов и перестраиваем модель
    void on_dateEdit_userDateChanged(const QDate &date);

    //Добавление сеанса
    void on_addSesion_clicked();
    //Редактирование сеанса
    void on_editSession_clicked();

    void on_saveDatabaseAction_triggered();

    void on_loadDatabaseAction_triggered();

    void on_closeAdminWindowAction_triggered();

    void on_deleteSession_clicked();

private:
    Ui::AdminWindow *ui;
    //Пример динамического связывания:
    //Указателю типа QAbstractItemModel будет присвоен адрес,
    //указывающий на класс-потомок - SessionModel
    QAbstractItemModel *sessionModel = nullptr;
    //Еще один пример динамического связывания
    //Указател типа QListView будет указывать на
    //Объект потомок, EmptyListView
    QListView *sessionListView;
    //Структура, в которой хранятся сеансы по датам
    QMultiHash<QDate,Session> sessionHashSet;
    //Текущий список
    QList<Session> sessionList;
    //Текущая дата
    QDate currentDate;
    //Выделенный сеанс
    Session sessionSelected;
    //Индекс в модели выделенного сеанса
    QModelIndex sessionSelectionIndex;
    //Имя текущего файла
    QString filename;

    //Метод установки текущего списка по дате из хеш-таблицы
    void setCurrentList();
    //Метод установки кнопок в неакт. состояние
    void setButtonUnactive();
};

#endif // ADMINWINDOW_H
