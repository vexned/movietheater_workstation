#ifndef CINEMAHALL_H
#define CINEMAHALL_H

#include <QList>
#include "seat.h"
#include <QDebug>
#include <QMetaType>

class CinemaHall
{
public:
    CinemaHall(int cols = 0, int SeatNumber = 0, const QString &name = "");
    CinemaHall(const CinemaHall &hall);

    CinemaHall& operator=(const CinemaHall &hall);
    ~CinemaHall();
    QString getName() const;
    int getRows() const;
    int getCols() const;
    QList<Seat> getSeats() const;
    bool operator ==(const CinemaHall& r);
    void setSeats(QList<Seat> s);
    void setName(const QString &value);

    int getSeatNumber() const;
    void setSeatNumber(int value);

    void setCols(int value);

    friend QDataStream &operator <<(QDataStream &stream, const CinemaHall &hall);
    friend QDataStream &operator >>(QDataStream &stream, CinemaHall &hall);

private:
    QList<Seat> seats; // Список мест
    QString name; //Название кинозала
    int rows; // ряды
    int cols; // кол-во мест на ряду (столбцы)
};

Q_DECLARE_METATYPE(CinemaHall)

#endif // CINEMAHALL_H
