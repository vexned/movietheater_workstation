#ifndef MOVIE_H
#define MOVIE_H

#include "displayobject.h"
#include <QDataStream>


enum class AgeRestriction {ZeroPlus, SixPlus, TwelvePlus, SixteenPlux, EighteenPlus};

class Movie : public DisplayObject
{
public:
    Movie(const QString &name = "", int duration = 90, AgeRestriction restriction = AgeRestriction::ZeroPlus);
    QString getRestrictionString(AgeRestriction res) const;
    AgeRestriction getRestriction() const;
    virtual QString getName() const;
    bool operator ==(const Movie& r);
    void setRestriction(const AgeRestriction &value);

    int getDuration() const;
    void setDuration(int value);

    friend QDataStream &operator<<(QDataStream &stream, const Movie &movie);
    friend QDataStream &operator>>(QDataStream &stream, Movie& movie);

private:
    QString arStr[5] = {"0+", "6+", "12+", "16+", "18+"};
    AgeRestriction restriction;
    int duration;
};

#endif // MOVIE_H
