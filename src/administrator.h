#ifndef ADMINISTRATOR_H
#define ADMINISTRATOR_H
#include "person.h"

class Administrator : public Person
{
public:
    Administrator();
    Administrator(const QString &fn, const QString &ln,const QString &mn,const QString &lg, const QString &pw);
    Administrator(const Administrator &admin);
    Administrator &operator =(const Administrator &admin);
    QString printName();
    virtual Privelege privelegeCheck(){return Privelege::Admin;}

    friend QDataStream &operator <<(QDataStream& stream, const Administrator& cashier);
    friend QDataStream &operator >>(QDataStream& stream, Administrator& cashier);

private:
};
#endif // ADMINISTRATOR_H
