#include "cinemahall.h"


CinemaHall::CinemaHall(int cols, int SeatNumber, const QString &name)
{
    for(int i = 0; i < cols * SeatNumber; i++)
    {
        seats.append(Seat(i));
    }
    this->name = name;
    this->rows = SeatNumber;
    this->cols = cols;
}

CinemaHall::CinemaHall(const CinemaHall &hall)
{
    int sizes = this->seats.size();

    for(int i = 0; i < sizes; i++)
        this->seats.removeAt(0);

    int size = hall.seats.size();
    for(int i = 0; i < size; i++)
    {
        this->seats.append(Seat(hall.seats.at(i)));
    }
    this->name = hall.name;
    this->cols = hall.cols;
    this->rows = hall.rows;
}

CinemaHall &CinemaHall::operator=(const CinemaHall &hall)
{
    int size = this->seats.size();
    for(int i = 0; i < size; i++)
        this->seats.removeAt(0);

    int size1 = hall.seats.size();
    for(int i = 0; i < size1; i++)
    {
        seats.append(Seat(hall.seats.at(i)));
    }
    name = hall.name;
    rows = hall.rows;
    cols = hall.cols;

    return *this;
}

CinemaHall::~CinemaHall()
{
    int size = seats.size();
    for(int i = 0; i < size; i++) {
        seats.removeAt(i);
    }
}

QString CinemaHall::getName() const
{
    return this->name;
}

int CinemaHall::getRows() const
{
    return this->cols;
}

int CinemaHall::getCols() const
{
    return this->rows;
}

QList<Seat> CinemaHall::getSeats() const
{
    return seats;
}

bool CinemaHall::operator ==(const CinemaHall &r)
{
    if(this->seats == r.seats && this->cols == r.cols && this->rows == r.rows && this->name == r.name) return true;
    return false;
}

void CinemaHall::setSeats(QList<Seat> s)
{
    this->seats = s;
}

void CinemaHall::setName(const QString &value)
{
    name = value;
}

int CinemaHall::getSeatNumber() const
{
    return rows;
}

void CinemaHall::setSeatNumber(int value)
{
    rows = value;
}

void CinemaHall::setCols(int value)
{
    cols = value;
}

QDataStream &operator >>(QDataStream &stream, CinemaHall &hall)
{
    return stream >> hall.seats >> hall.rows >> hall.cols >> hall.name;
}

QDataStream &operator <<(QDataStream &stream, const CinemaHall &hall)
{
    return stream << hall.seats << hall.rows << hall.cols << hall.name;
}
