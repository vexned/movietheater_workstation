#ifndef CINEMAHALLMODEL_H
#define CINEMAHALLMODEL_H

#include <QFont>
#include <QBrush>
#include <QAbstractTableModel>
#include "cinemahall.h"

enum CinemaHallModelRole { getSeat = Qt::UserRole+1,
                           changeSeat = Qt::UserRole+2,
                           getHall = Qt::UserRole+3};

class CinemaHallModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    CinemaHallModel(CinemaHall &hall ,QObject *parent = 0)
        : QAbstractTableModel(parent), hall(hall) {}
    //Перегруженный виртуальные функции
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    //
private:
    //Кинозал
    CinemaHall hall;
signals:
    void editCompleted(const Seat&);
};

#endif // CINEMAHALLMODEL_H
