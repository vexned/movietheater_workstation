#include "cinemahallmodel.h"

int CinemaHallModel::rowCount(const QModelIndex &parent) const
{
    //Количество рядок в кинозале
    return hall.getRows();
}

int CinemaHallModel::columnCount(const QModelIndex &parent) const
{
    //Количество мест(столбцов) на ряд
    return hall.getCols();
}

QVariant CinemaHallModel::data(const QModelIndex &index, int role) const
{
    switch(role) {
    //Выведение номера мест
    case Qt::DisplayRole:
        return QString("%1").arg((hall.getCols()*index.row())+index.column()+1);
        break;
    //Выведение цвета места, в зависимости от состояния
    case Qt::BackgroundRole: {
        QList<Seat> s = hall.getSeats();
        switch(s.at(hall.getCols()*(index.row())+index.column()).getSeatState()) {
        case SeatState::Avalaible: {
            QBrush blue(Qt::blue);
            return blue;
        } break;
        case SeatState::Booked: {
            QBrush white(Qt::white);
            return white;
        } break;
        case SeatState::Bought: {
            QBrush red(Qt::red);
            return red;
        } break;
        }
    }    break;
    case CinemaHallModelRole::getSeat: {
        QList<Seat> ss = hall.getSeats();
        return QVariant::fromValue(ss.at(hall.getCols()*(index.row())+index.column()));
    } break;

    case CinemaHallModelRole::getHall: {
        return QVariant::fromValue(hall);
    } break;
    }
    return QVariant();
}

QVariant CinemaHallModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    //Выведение номера ряда
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Vertical) {
           return QString("Ряд %1").arg(section+1);
        }
    }
    return QVariant();
}

bool CinemaHallModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(role == CinemaHallModelRole::changeSeat){
        QList<Seat> seats = hall.getSeats();
        seats.replace(hall.getCols()*(index.row())+index.column(),value.value<Seat>());
        hall.setSeats(seats);
    }
    return true;
}

Qt::ItemFlags CinemaHallModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}


