#include "session.h"

void Session::setMovie(const Movie &value)
{
    movie = value;
}

void Session::setTime(const QTime &value)
{
    time = value;
}

Session &Session::operator=(const Session &session)
{
    this->movie = session.movie;
    this->hall = session.hall;
    this->time = session.time;
    return *this;
}

QDataStream &operator <<(QDataStream &stream, const Session &session)
{
    return stream << session.hall << session.movie << session.time;
}

QDataStream &operator >>(QDataStream &stream, Session &session)
{
    return stream >> session.hall >> session.movie >> session.time;
}

Session::Session(Movie movie, CinemaHall hall, QTime time)
{
    this->movie = movie;
    this->hall = hall;
    this->time = time;
}

Session::Session()
{
}

QString Session::getSessionInfo() const
{
    return QString(movie.getName()+" ("+movie.getRestrictionString(movie.getRestriction())+") "+hall.getName());
}

Movie Session::getMovie() const
{
    return movie;
}

CinemaHall Session::getCinemaHall() const
{
    return hall;
}

QTime Session::getTime() const
{
    return time;
}

void Session::setCinemaHall(CinemaHall c) const
{
    this->hall = c;
}

bool Session::operator ==(const Session &r)
{
    return (this->hall == r.hall && this->movie == r.movie && this->time == r.time)? true : false;
}

