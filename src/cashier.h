#ifndef CASHIER_H
#define CASHIER_H

#include "person.h"

class Cashier : public Person
{
public:
    Cashier();
    Cashier(const QString &fn, const QString &ln,const QString &mn,const QString &lg,const QString &pw);
    Cashier(const Cashier &cashier);
    Cashier &operator =(const Cashier &cashier);
    void addCash(const double &cash);
    void removeCash(const double &cash);
    QString printName();
    virtual Privelege privelegeCheck(){return Privelege::Cashier;}

    friend QDataStream &operator <<(QDataStream& stream, const Cashier& cashier);
    friend QDataStream &operator >>(QDataStream& stream, Cashier& cashier);
    double getCashForDay() const;
    void setCashForDay(double value);

private:
    double CashForDay;
};

#endif // CASHIER_H
