#ifndef EDITSESSIONDIALOG_H
#define EDITSESSIONDIALOG_H

#include <QDialog>
#include <QMap>
#include <QHash>
#include <QPair>
#include "session.h"

namespace Ui {
class EditSessionDialog;
}

class EditSessionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditSessionDialog(Session *session, QWidget *parent = 0);
    ~EditSessionDialog();

private slots:
    void on_buttonBox_accepted();
signals:
    void applyChanges();

private:
    Ui::EditSessionDialog *ui;
    QMap<QString, AgeRestriction> comboValues;
    Movie movie;
    CinemaHall hall;
    QTime time;
    Session *curS;
};

#endif // EDITSESSIONDIALOG_H
