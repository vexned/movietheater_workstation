#include "movie.h"

Movie::Movie(const QString &name, int duration, AgeRestriction res)
    : DisplayObject(name)
{
    restriction = res;
    this->duration = duration;
}

QString Movie::getRestrictionString(AgeRestriction res) const
{
    return arStr[static_cast<int>(res)];
}

AgeRestriction Movie::getRestriction() const
{
    return this->restriction;
}

QString Movie::getName() const
{
    return objectName;
}

bool Movie::operator ==(const Movie &r)
{
    if(this->getName() == r.getName() && this->restriction == r.restriction) return true;
    return false;
}

void Movie::setRestriction(const AgeRestriction &value)
{
    restriction = value;
}

int Movie::getDuration() const
{
    return duration;
}

void Movie::setDuration(int value)
{
    duration = value;
}

QDataStream &operator<<(QDataStream &stream, const Movie &movie)
{
    stream << (DisplayObject)movie;
    stream << movie.duration;
    int rest = static_cast<int>(movie.restriction);
    stream << rest;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Movie &movie)
{
//    QString name;
//    stream >> name;
//    movie.setName(name);
    stream >> static_cast<DisplayObject&>(movie);
    stream >> movie.duration;
    int rest;
    stream >> rest;
    movie.restriction = static_cast<AgeRestriction>(rest);
    return stream;
}
